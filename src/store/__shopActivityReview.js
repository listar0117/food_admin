
export default {
  state: {
    current: [],
    seen: [],
    unSeen: []
  },
  mutations: {
    setShopActivityReviewData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopActivityReviewData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    shopActivityReviewSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length.toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getShopActivityReviewData ({ state }) {
      let vm = this
      vm.commit('emptyShopActivityReviewData')
      axios.get('/json/shop-activity-review.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopActivityReviewData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
