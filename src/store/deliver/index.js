
import shop from './__deliverShop'
import order from './__deliverOrder'

export default {
  namespaced: true,
  modules: {
    order, shop
  }
}
