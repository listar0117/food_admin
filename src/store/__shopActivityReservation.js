
export default {
  state: {
    current: [],
    denied: [],
    onHold: []
  },
  mutations: {
    setShopActivityReservationData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopActivityReservationData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    shopActivityReservationSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length.toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getShopActivityReservationData ({ state }) {
      let vm = this
      vm.commit('emptyShopActivityReservationData')
      axios.get('/json/shop-activity-reservation.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopActivityReservationData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
