
export default {
  state: {
    current: [],
    denied: [],
    onHold: []
  },
  mutations: {
    setRestaurantReservationData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyRestaurantReservationData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    restaurantReservationSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length.toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getRestaurantReservationData ({ state }) {
      let vm = this
      vm.commit('restaurant/emptyRestaurantReservationData')
      axios.get('/json/restaurant-reservation.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('restaurant/setRestaurantReservationData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
