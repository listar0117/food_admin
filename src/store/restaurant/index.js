
import reservation from './__restaurantReservation'
import shop from './__restaurantShop'

export default {
  namespaced: true,
  modules: {
    reservation, shop
  }
}
