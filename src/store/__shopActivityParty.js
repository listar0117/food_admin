
export default {
  state: {
    current: [],
    canceld: []
  },
  mutations: {
    setShopActivityPartyData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopActivityPartyData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    shopActivityPartySummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (key === 'canceld') {
              let total = 0
              Object.keys(state).map(key2 => {
                total += state[key2].length
              })
              let parcent = (data[key] / total) * 100
              data[key] = parcent
            }
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      data.total = 0
      Object.keys(state).map(key => {
        if (Array.isArray(state[key])) {
          data.total += state[key].length
        }
      })
      data.total = data.total.toLocaleString()
      return data
    }
  },
  actions: {
    getShopActivityPartyData ({ state }) {
      let vm = this
      vm.commit('emptyShopActivityPartyData')
      axios.get('/json/deliver-order.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopActivityPartyData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
