
export default {
  state: {
    datas: []
  },
  mutations: {
    setShopExtraServiceData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopExtraServiceData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopExtraServiceData ({ state }) {
      let vm = this
      vm.commit('emptyShopExtraServiceData')
      axios.get('/json/shop-extra-service.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopExtraServiceData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
