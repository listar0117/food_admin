
export default {
  state: {
    datas: []
  },
  mutations: {
    setPaymentPayoutData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyPaymentPayoutData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getPaymentPayoutData ({ state }) {
      let vm = this
      vm.commit('emptyPaymentPayoutData')
      axios.get('/json/payment-payout.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setPaymentPayoutData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
