
export default {
  state: {
    datas: []
  },
  mutations: {
    setShopExtraAllergyData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopExtraAllergyData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopExtraAllergyData ({ state }) {
      let vm = this
      vm.commit('emptyShopExtraAllergyData')
      axios.get('/json/shop-extra-allergy.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopExtraAllergyData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
