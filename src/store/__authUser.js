
import gql from 'graphql-tag'

export default {
  state: {
    first_name: '',
    last_name: '',
    avater: '',
    designation: '',
    memberFrom: '',
    is_online: false
  },
  mutations: {
    setUserInfo (state, data) {
      Object.keys(state).map(key => {
        state[key] = data[key]
        return state[key]
      })
    }
  },
  actions: {
    async checkUserInfo ({ state }, payload) {
      let vm = this
      let adminResponse = await payload.vm.$apollo.query({
        // Query
        query: gql`{
              authUser {
                id
                first_name
                last_name
                email
              }
          }`
      })
      let userData = adminResponse.data.authUser
      userData.is_online = true
      userData.avater = '/images/user-dave-robert-35x35.png'
      vm.commit('setUserInfo', userData)
    }
  }
}
