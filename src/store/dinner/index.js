
import shop from './__dinnerShop'
import order from './__dinnerOrder'

export default {
  namespaced: true,
  modules: {
    order, shop
  }
}
