
export default {
  state: {
    items: [],
    category: []
  },
  mutations: {
    setShopExtraFoodData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopExtraFoodData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopExtraFoodData ({ state }) {
      let vm = this
      vm.commit('emptyShopExtraFoodData')
      axios.get('/json/shop-extra-food.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopExtraFoodData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
