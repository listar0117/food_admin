
export default {
  state: {
    deliveryIncome: [],
    restaurantIncome: [],
    partyServiceIncome: [],
    dinnersIncome: []
  },
  mutations: {
    setShopIncomeData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopIncomeData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopIncomeData ({ state }) {
      let vm = this
      vm.commit('income/emptyShopIncomeData')
      axios.get('/json/income/shop-income.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('income/setShopIncomeData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
