
import shopIncome from './__shopIncome'
import yamiyamiIncome from './__yamiyamiIncome'

export default {
  namespaced: true,
  modules: {
    yamiyamiIncome, shopIncome
  }
}
