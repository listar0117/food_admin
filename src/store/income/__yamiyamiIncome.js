
export default {
  state: {
    yearly: [],
    monthly: []
  },
  mutations: {
    setYamiyamiIncomeData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyYamiyamiIncomeData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getYamiyamiIncomeData ({ state }) {
      let vm = this
      vm.commit('income/emptyYamiyamiIncomeData')
      axios.get('/json/income/yamiyami-income.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('income/setYamiyamiIncomeData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
