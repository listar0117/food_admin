
export default {
  state: {
    new: [],
    active: [],
    closed: []
  },
  mutations: {
    setTicketUserData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyTicketUserData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    ticketUserSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (key === 'active') {
              let total = 0
              Object.keys(state).map(key2 => {
                total += state[key2].length
              })
              let parcent = (data[key] / total) * 100
              data[key] = parcent
            }
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getTicketUserData ({ state }) {
      let vm = this
      vm.commit('reports/emptyTicketUserData')
      axios.get('/json/tickets-user.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('reports/setTicketUserData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
