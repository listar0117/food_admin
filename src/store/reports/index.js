
import ticketShop from './__ticketShop'
import ticketUser from './__ticketUser'

export default {
  namespaced: true,
  modules: {
    ticketUser, ticketShop
  }
}
