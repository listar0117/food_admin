
export default {
  state: {
    new: [],
    active: [],
    closed: []
  },
  mutations: {
    setTicketShopData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyTicketShopData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    ticketShopSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (key === 'active') {
              let total = 0
              Object.keys(state).map(key2 => {
                total += state[key2].length
              })
              let parcent = (data[key] / total) * 100
              data[key] = parcent
            }
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getTicketShopData ({ state }) {
      let vm = this
      vm.commit('reports/emptyTicketShopData')
      axios.get('/json/tickets-shop.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('reports/setTicketShopData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
