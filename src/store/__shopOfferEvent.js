
export default {
  state: {
    active: [],
    onHold: []
  },
  mutations: {
    setShopOfferEventData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopOfferEventData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    shopOfferEventSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      data.total = 0
      Object.keys(state).map(key => {
        if (Array.isArray(state[key])) {
          data.total += state[key].length
        }
      })
      data.total = data.total.toLocaleString()
      return data
    }
  },
  actions: {
    getShopOfferEventData ({ state }) {
      let vm = this
      vm.commit('emptyShopOfferEventData')
      axios.get('/json/shop-offer-event.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopOfferEventData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
