
import popupSettings from './__popupSettings'
import shopDiscountManage from './__shopDiscountManage'

export default {
  namespaced: true,
  modules: {
    popupSettings, shopDiscountManage
  }
}
