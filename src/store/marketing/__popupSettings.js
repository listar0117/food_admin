
export default {
  state: {
    datas: []
  },
  mutations: {
    setPopupSettingsData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyPopupSettingsData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getPopupSettingsData ({ state }) {
      let vm = this
      vm.commit('marketing/emptyPopupSettingsData')
      axios.get('/json/marketing/popup-settings.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('marketing/setPopupSettingsData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
