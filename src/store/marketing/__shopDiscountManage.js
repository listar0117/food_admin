
export default {
  state: {
    datas: []
  },
  mutations: {
    setShopDiscountManageData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopDiscountManageData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopDiscountManageData ({ state }) {
      let vm = this
      vm.commit('marketing/emptyShopDiscountManageData')
      axios.get('/json/marketing/shop-discount-manage.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('marketing/setShopDiscountManageData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
