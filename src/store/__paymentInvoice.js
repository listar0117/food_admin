
export default {
  state: {
    new: [],
    accepted: [],
    denied: [],
    onHold: []
  },
  mutations: {
    setPaymentInvoiceData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyPaymentInvoiceData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    paymentInvoiceSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length.toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getPaymentInvoiceData ({ state }) {
      let vm = this
      vm.commit('emptyPaymentInvoiceData')
      axios.get('/json/requests.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setPaymentInvoiceData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
