
export default {
  state: {
    active: [],
    closed: [],
    suspended: []
  },
  mutations: {
    setPartyServiceShopData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyPartyServiceShopData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    partyServiceShopSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      data.total = 0
      Object.keys(state).map(key => {
        if (Array.isArray(state[key])) {
          data.total += state[key].length
        }
      })
      data.total = data.total.toLocaleString()
      return data
    }
  },
  actions: {
    getPartyServiceShopData ({ state }) {
      let vm = this
      vm.commit('party/emptyPartyServiceShopData')
      axios.get('/json/dinner-shop.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('party/setPartyServiceShopData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
