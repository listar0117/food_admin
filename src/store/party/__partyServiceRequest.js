
export default {
  state: {
    new: [],
    denied: [],
    onHold: []
  },
  mutations: {
    setPartyServiceRequestData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyPartyServiceRequestData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    partyServiceRequestSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length
            if (!data[key]) data[key] = 0
            data[key] = data[key].toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getPartyServiceRequestData ({ state }) {
      let vm = this
      vm.commit('party/emptyPartyServiceRequestData')
      axios.get('/json/party-service-request.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('party/setPartyServiceRequestData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
