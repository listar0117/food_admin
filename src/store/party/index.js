
import shop from './__partyServiceShop'
import bids from './__partyServiceBids'
import booking from './__partyServiceBooking'
import request from './__partyServiceRequest'

export default {
  namespaced: true,
  modules: {
    shop, bids, booking, request
  }
}
