
export default {
  state: {
    new: [],
    accepted: [],
    denied: [],
    onHold: []
  },
  mutations: {
    setRequestData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyRequestData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  getters: {
    requestsSummery: state => {
      let data = {}
      if (state) {
        Object.keys(state).map(key => {
          if (Array.isArray(state[key])) {
            data[key] = state[key].length.toLocaleString()
            return data[key]
          }
        })
      }
      return data
    }
  },
  actions: {
    getRequestData ({ state }) {
      let vm = this
      vm.commit('emptyRequestData')
      axios.get('/json/requests.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setRequestData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
