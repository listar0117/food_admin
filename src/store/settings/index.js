
import menuImage from './__menuImage'
import settingsAdmin from './__settingsAdmin'
import websiteLanguage from './__websiteLanguage'

export default {
  namespaced: true,
  modules: {
    websiteLanguage, menuImage, settingsAdmin
  }
}
