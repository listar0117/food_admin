import gql from 'graphql-tag'
import { apolloQuery, apolloMutate } from '@/vue-functions'

export default {
  state: {
    admins: [],
    errors: {}
  },
  mutations: {
    setSettingsAdminData (state, data) {
      state.admins = data.admins
    },
    setSettingsAdminErrorData (state, data) {
      state.errors = data.errors
    },
    emptySettingsAdminData (state) {
      state.admins = []
      state.errors = {}
    }
  },
  actions: {
    async getSettingsAdminData ({ state }, payload) {
      let vmStore = this
      vmStore.commit('settings/emptySettingsAdminData')
      let adminsList = await apolloQuery(
        payload.vm, 'admins', `
          id
          first_name
          last_name
          services
          image
          email
          telephone_number
      `)
      .catch(error => {})
      if(adminsList) vmStore.commit('settings/setSettingsAdminData', adminsList)
    },
    async saveSettingsAdminData ({ state }, payload) {
      let vmStore = this
      let adminsList = false

      // Save Existing Data
      if (!payload.adminId) {
        adminsList = await apolloMutate(
          payload.vm, 'createAdmin', `
            id
            first_name
            last_name
            services
            image
            email
            telephone_number`,
          {
            first_name: payload.adminData.first_name,
            last_name: payload.adminData.last_name,
            services: payload.adminData.services,
            image: payload.adminData.image,
            email: payload.adminData.email,
            password: payload.adminData.password,
            telephone_number: payload.adminData.telephone_number
          }
        )
        .catch(error => {
          if(error) vmStore.commit('settings/setSettingsAdminErrorData', { errors: error })
        })
        if (adminsList) $.notify('Congratulation, new Admin Account created', { className: "success", globalPosition: "right bottom" })
      } else {
        adminsList = await apolloMutate(
          payload.vm, 'saveAdmin', `
            id
            first_name
            last_name
            services
            image
            email
            telephone_number`,
          {
            id: payload.adminId,
            first_name: payload.adminData.first_name,
            last_name: payload.adminData.last_name,
            services: payload.adminData.services,
            image: payload.adminData.image,
            email: payload.adminData.email,
            password: payload.adminData.password,
            telephone_number: payload.adminData.telephone_number
          }
        )
        .catch(error => {
          if(error) vmStore.commit('settings/setSettingsAdminErrorData', { errors: error })
        })
        if (adminsList) $.notify('Admin data saved', { className: "success", globalPosition: "right bottom" })
      }
      if(adminsList) {
        payload.vm.$emit('closeModal')
        vmStore.commit('settings/setSettingsAdminData', {admins: adminsList})
      }
    },
    async removeSettingsAdminData ({ state }, payload) {
      let listDatas = []
      let adminsList = await apolloMutate(
          payload.vm, 'deleteAdmin', `
            id
            first_name
            last_name
            services
            image
            email
            telephone_number`,
          {
            id: payload.adminId
          }
        )
        .catch(error => {})
      if(adminsList) {
        this.commit('settings/setSettingsAdminData', {admins: adminsList})
        $.notify('Admin Account deleted', { className: "success", globalPosition: "right bottom" })
      }
    }
  }
}
