
export default {
  state: {
    menuCategory: [],
    cardItem: [],
    itemType: []
  },
  mutations: {
    setMenuImageData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyMenuImageData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getMenuImageData ({ state }) {
      let vm = this
      vm.commit('settings/emptyMenuImageData')
      axios.get('/json/settings/menu-images.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('settings/setMenuImageData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
