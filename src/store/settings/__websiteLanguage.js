
export default {
  state: {
    datas: []
  },
  mutations: {
    setWebsiteLanguageData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyWebsiteLanguageData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getWebsiteLanguageData ({ state }) {
      let vm = this
      vm.commit('settings/emptyWebsiteLanguageData')
      axios.get('/json/settings/website-language.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('settings/setWebsiteLanguageData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
