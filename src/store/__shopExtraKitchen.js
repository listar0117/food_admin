
export default {
  state: {
    datas: []
  },
  mutations: {
    setShopExtraKitchenData (state, data) {
      Object.keys(state).map(key => {
        if (Array.isArray(data[key])) {
          state[key] = data[key]
          return state[key]
        }
      })
    },
    emptyShopExtraKitchenData (state) {
      Object.keys(state).map(key => {
        state[key] = []
        return state[key]
      })
    }
  },
  actions: {
    getShopExtraKitchenData ({ state }) {
      let vm = this
      vm.commit('emptyShopExtraKitchenData')
      axios.get('/json/shop-extra-kitchen.json')
        .then(response => {
          // Simulate server delay
          setTimeout(function () {
            if (response.data) { vm.commit('setShopExtraKitchenData', response.data) }
          }, 500)
        })
        .catch(error => {
          // Error handling
          console.log('Check for auth permission /n' + error.response)
        })
    }
  }
}
