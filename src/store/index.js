
import Vue from 'vue'
import Vuex from 'vuex'

import authUser from './__authUser'
import requests from './__requests'
import posSystem from './__posSystem'
import paymentWay from './__paymentWay'
import customerUser from './__customerUser'
import shopOfferJob from './__shopOfferJob'
import shopOfferFood from './__shopOfferFood'
import paymentPayout from './__paymentPayout'
import customerClient from './__customerClient'
import shopOfferOffer from './__shopOfferOffer'
import shopOfferEvent from './__shopOfferEvent'
import shopOfferCoupon from './__shopOfferCoupon'
import shopActivityTakeway from './__shopActivityTakeway'
import shopActivityOrder from './__shopActivityOrder'
import shopActivityReservation from './__shopActivityReservation'
import shopOfferStempcard from './__shopOfferStempcard'
import shopActivityParty from './__shopActivityParty'
import paymentInvoice from './__paymentInvoice'
import shopExtraAllergy from './__shopExtraAllergy'
import shopExtraKitchen from './__shopExtraKitchen'
import shopExtraAdditive from './__shopExtraAdditive'
import shopActivityReview from './__shopActivityReview'
import shopExtraService from './__shopExtraService'
import shopExtraFood from './__shopExtraFood'
import shopExtraParty from './__shopExtraParty'

import reports from './reports'
import deliver from './deliver'
import dinner from './dinner'
import restaurant from './restaurant'
import party from './party'
import income from './income'
import marketing from './marketing'
import settings from './settings'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    requests,
    authUser,
    posSystem,
    shopOfferCoupon,
    shopOfferFood,
    shopOfferJob,
    shopOfferEvent,
    shopOfferStempcard,
    shopOfferOffer,
    paymentPayout,
    paymentWay,
    paymentInvoice,
    customerClient,
    customerUser,
    shopActivityOrder,
    shopActivityTakeway,
    shopActivityReservation,
    shopActivityParty,
    shopActivityReview,
    shopExtraAllergy,
    shopExtraAdditive,
    shopExtraKitchen,
    shopExtraService,
    shopExtraFood,
    shopExtraParty,

    reports,
    deliver,
    dinner,
    restaurant,
    party,
    income,
    marketing,
    settings
  }
})
