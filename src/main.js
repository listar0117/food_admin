import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// CSS Assets
import '@/styles/init.scss'

// JS Core Plugins
import jQuery from 'jquery'
import axios from 'axios'
import _ from 'underscore'
import dt from 'datatables.net'
import Raphael from 'raphael'
import gql from 'graphql-tag'
import { createProvider } from './vue-apollo'

global.$ = global.jQuery = jQuery
global.Raphael = Raphael
global.axios = axios
global._ = _
global.dt = dt
global.gql = gql

// Js Plugins
require('bootstrap')
require('bootstrap-notify')

require('jquery-knob')
require('jquery-sparkline')
require('jquery-slimscroll')

require('admin-lte')
require('datatables.net')
require('datatables.net-bs')
require('select2')
require('icheck')
require('morris.js/morris.js')
require('inputmask/dist/jquery.inputmask.bundle.js')

require('admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')
require('admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')

// Local Script
require('./scripts/userSettings.js')
require('./scripts/notify.js')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
