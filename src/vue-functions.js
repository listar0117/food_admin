// Manually call this when user log out
import gql from 'graphql-tag'
import { handleErrors } from '@/vue-error'

export async function apolloQuery (vm, queryName, fields) {
  let responseData = false
  await vm.$apollo.query({
    query: gql `query { ${queryName} { ${fields} } }`
  })
  .then(response => {
    let { data } = response
    responseData = data
    // $.notify('Data loaded', { className: "success", globalPosition: "right bottom" })
  })
  .catch(error => {
    let { graphQLErrors } = error
    let formErrors = handleErrors(vm, graphQLErrors)
    if(formErrors) throw formErrors
  })
  return responseData
}

export async function apolloMutate (vm, queryName, fields, queryVariables = {}) {
  let responseData = false
  let mutateVars = ``
  let mutateFnVars = ``
  let mutateVarsVal = {}
  let keyCount = 0
  let keys = Object.keys(queryVariables).length

  Object.keys(queryVariables)
  .forEach(function eachKey(key) {
    keyCount++
    
    if(!queryVariables[key]) queryVariables[key] = ''

    let varType = typeof queryVariables[key]
    let varTypeCapitalize = varType.charAt(0).toUpperCase()
    varTypeCapitalize += varType.slice(1)

    mutateVars += `$${key}: ${varTypeCapitalize}`
    if(keyCount < keys) mutateVars += `, `

    mutateFnVars += `${key}: $${key}`
    if(keyCount < keys) mutateFnVars += `, `

    mutateVarsVal[key] = queryVariables[key]
  })

  await vm.$apollo.mutate({
    mutation: gql `mutation(${mutateVars}) { ${queryName} (${mutateFnVars}) { ${fields} } }`,
    variables: mutateVarsVal
  })
  .then(response => {
    let { data } = response
    responseData = data[queryName]
    // $.notify('Data loaded', { className: "success", globalPosition: "right bottom" })
  })
  .catch(error => {
    let { graphQLErrors } = error
    let formErrors = handleErrors(vm, graphQLErrors)
    if(formErrors) throw formErrors
  })
  return responseData
}

